# Tuna Egg Cups

## Serves
1

## Ingredients
2 Eggs
1 Canned Tuna

## Prep
1. Put pot on stove
2. Turn on stove

## Instructions
1. Put eggs in pot
2. Wait for water to boil
3. Let eggs boil for 1-2 minutes
4. Take pot off stove and run the eggs under cold water
5. Serve eggs
6. Break and scoop tops of eggs with a spoon to reveal runny yolk
7. Put tuna in runny yolk
8. Enjoy!
