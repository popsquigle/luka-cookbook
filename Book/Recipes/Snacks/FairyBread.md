# Fairy Bread

## Serves
1

## Ingredients
2 Pieces of bread
1 Butter Tub (don't worry we arent using the whole thing)
1 Sprinkles Jar (again, not the whole thing)

## Instructions
1. Put bread on plate
2. Pickup and butter both pieces of bread
3. Shake sprinkles onto bread
4. Enjoy
