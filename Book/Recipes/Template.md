# Title

## Serves
{serves}

## Ingredients
{ingredients}

## Prep
{prep steps}

## Instructions
{instructions, make sure to include 'Enjoy!' at the end!}
